/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author user
 */
public class DeleteUser {
         public static void main( String args[] ) {
      Connection c = null;
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         c = DriverManager.getConnection("jdbc:sqlite:user.db");
         c.setAutoCommit(false);
         System.out.println("Opened database successfully");

         stmt = c.createStatement();
         String sql = "DELETE from user where ID=4;";
         stmt.executeUpdate(sql);
         sql = "DELETE from user where ID=5;";
         stmt.executeUpdate(sql);
         sql = "DELETE from user where ID=6;";
         stmt.executeUpdate(sql);
         c.commit();

         ResultSet rs = stmt.executeQuery( "SELECT * FROM user;" );
         
         while ( rs.next() ) {
          int id = rs.getInt("id");
         String  username = rs.getString("username");
         String  password = rs.getString("password");
         
         System.out.println( "id = " + id );
         System.out.println( "username = " + username );
         System.out.println( "password = " + password );
         System.out.println();
      }
      rs.close();
      stmt.close();
      c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Operation done successfully");
   }
}
