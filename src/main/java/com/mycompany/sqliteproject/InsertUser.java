/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *
 * @author user
 */
public class InsertUser {

    public static void main( String args[] ) {
      Connection c = null;
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         c = DriverManager.getConnection("jdbc:sqlite:user.db");
         c.setAutoCommit(false);
         System.out.println("Opened database successfully");

         stmt = c.createStatement();
         String sql = "INSERT INTO user (id,username,password)"+
                        "VALUES (3, 'Captain','password');"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO user (id,username,password)"+
                        "VALUES (5, 'Fair','password');"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO user (id,username,password)"+
                        "VALUES (6, 'Ice','password');"; 
         stmt.executeUpdate(sql);

         c.commit();
         stmt.close();
         c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Records created successfully");
   }
} 



